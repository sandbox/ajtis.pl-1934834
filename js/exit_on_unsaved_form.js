(function ($) {

    Drupal.behaviors.myModuleBehavior = {
        attach: function (context, settings) {
            if (typeof shouldBlock === 'undefined') var shouldBlock = false;
            $('input , textarea , select').change(function () {
                $(this).addClass(typeof exit_on_unsaved_class_name!= 'undefined' ? exit_on_unsaved_class_name.toString() : 'changed');
                shouldBlock = true;
            });
            $('input[type=submit]').click(function () {
                shouldBlock = false;
            });
            if (exit_on_unsaved_trigger_overlay_close == true) {
                $('.overlay-close').click(function (event) {
                    if (shouldBlock) {
                        var r = window.confirm(typeof exit_on_unsaved_notice_text != 'undefined' ? exit_on_unsaved_notice_text.toString() : Drupal.t('You have unsaved changes ...'));
                        if (r) {

                        }
                        else {
                            event.stopPropagation();
                        }
                    }
                });
            }
            if (exit_on_unsaved_trigger_backspace_keyup == true) {
                $('html').keydown(function (e) {
                    var code = e.keyCode || e.which
                    if (code == 8) {
                        var hasFocus=jQuery('input:focus',this).length;
                        if (shouldBlock && hasFocus==0) {
                            var r = window.confirm(typeof exit_on_unsaved_notice_text != 'undefined' ? exit_on_unsaved_notice_text.toString() : Drupal.t('You have unsaved changes ...'));
                            if (r) {

                            }
                            else {
                                event.stopPropagation();
                                return;
                            }
                        }
                    }
                });
            }
            if (exit_on_unsaved_trigger_window_close == true) {
                $(window).bind('beforeunload', function () {
                    if (shouldBlock) {
                        return typeof exit_on_unsaved_notice_text != 'undefined' ? exit_on_unsaved_notice_text.toString() : Drupal.t('You have unsaved changes ...');
                    }
                });
            }
        }
    };

}(jQuery));
